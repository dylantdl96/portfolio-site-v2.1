# Portfolio Site v2.1 (Gulp, Nunjucks, SASS)

 Project using [Nunjucks](http://mozilla.github.io/nunjucks/),[SASS](http://sass-lang.com/) and [Gulp](http://gulpjs.com/). Browsersync runs out of 'dist' folder and watches all Nunjucks templates, SASS and JS and live reloads in browser on change. Also worked in some img min and moving fonts and assets to a 'dist' folder for easy deployment.


Notable packages required:

- gulp
- gulp-cli
- sass
- gulp-sass
- gulp-postcss
- autoprefixer
- cssnano
- gulp-babel
- gulp-terser
- browser-sync
- gulp-nunjucks-render
- gulp-imagemin
- imagemin-pngquant


## Setup

1) Install [Gulp](http://gulpjs.com/) and [NPM](http://nodejs.org) if you do not already have them.

2) Install npm dependencies
- If  `package-lock.json`  does not exist:
```
npm install
```
    
- If  `package-lock.json`  file exists:

```
npm ci
```
3) Run Gulp
```
gulp
```

