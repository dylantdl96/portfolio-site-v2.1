// Initialize modules
const { src, dest, watch, series } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const babel = require('gulp-babel');
const terser = require('gulp-terser');
const browsersync = require('browser-sync').create();
const nunjucksRender = require('gulp-nunjucks-render');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');

// Use dart-sass for @use
// sass.compiler = require('dart-sass');

// Sass Task
function scssTask() {
    return src('app/scss/styles.scss', { sourcemaps: true })
        .pipe(sass())
        .pipe(postcss([autoprefixer(), cssnano()]))
        .pipe(dest('dist', { sourcemaps: '.' }));
}

// JavaScript Task
function jsTask() {
    return src('app/js/script.js', { sourcemaps: true })
        .pipe(babel({ presets: ['@babel/preset-env'] }))
        .pipe(terser())
        .pipe(dest('dist', { sourcemaps: '.' }));
}

// External Task
function externalFilesTask() {
    return src(['app/external/**/*'])
        .pipe(dest('dist/external/')
        );
}

// Nunjuck Task
function nunjuckTask() {
    nunjucksRender.nunjucks.configure(['./templates/*']);

    return src('./pages/*.njk')
        // Renders template with nunjucks
        .pipe(nunjucksRender())
        // output files in dist folder
        .pipe(dest('dist', { sourcemaps: '.' }))
}

// Imagemin Task
function imageMinTask() {
    return src('app/assets/**/*')
        .pipe(
            imagemin({
                progressive: true,
                svgoPlugins: [{ removeViewBox: false }],
                use: [pngquant()],
            })
        )
        .pipe(dest('dist/assets/'));
}

// Browsersync
function browserSyncServe(cb) {
    browsersync.init({
        server: {
            baseDir: './dist',
            serveStaticOptions: {
                extensions: ['html']
            }
        },
        notify: {
            styles: {
                top: 'auto',
                bottom: '0',
            },
        },
    });
    cb();
}
function browserSyncReload(cb) {
    browsersync.reload();
    cb();
}

// Watch Task
function watchTask() {
    watch([
        'pages/*.njk',
        'templates/*.njk',
        'templates/**/*.njk',
    ],
        series(nunjuckTask, browserSyncReload));
    watch(
        ['app/scss/**/*.scss', 'app/**/*.js'],
        series(scssTask, jsTask, browserSyncReload)
    );
    watch(
        'app/assets/**/*',
        series(imageMinTask, browserSyncReload)
    );
}

// Default Gulp Task
exports.default = series(scssTask, jsTask, externalFilesTask, nunjuckTask, imageMinTask, browserSyncServe, watchTask);

// Build Gulp Task
exports.build = series(scssTask, jsTask, externalFilesTask, nunjuckTask, imageMinTask);